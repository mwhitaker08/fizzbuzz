<?php include('./form.php'); ?>
<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Matthew Whitaker">
    <title>FizzBuzz - Matthew Whitaker</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/cover.css" rel="stylesheet">
  </head>
  <body class="d-flex h-100 text-center text-white bg-dark">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="mb-auto">
        <div>
            <h3 class="mb-0">FizzBuzz</h3>
        </div>
    </header>

    <main class="px-3 mb-5">
        <form name="stage_form" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
            <select name="stage" class="form-select">
                <option value="1" <?php if(isset($_POST['stage']) && $_POST['stage'] == 1) echo "SELECTED"; ?>>Stage 1</option>
                <option value="2" <?php if(isset($_POST['stage']) && $_POST['stage'] == 2) echo "SELECTED"; ?>>Stage 2</option>
            </select>
            <input type="submit" name="submit" class="btn btn-lg btn-secondary fw-bold border-white bg-white mt-5" value="Submit" />
        </form>

        <div class="px-3 mt-5 results">
          <?php if(isset($createNums)){ ?>
            <p>Numbers: <?php echo $createNums->number1." & ".$createNums->number2; ?></p>
          <?php } ?>
          <ul>
          <?php
            if(isset($fbResult))
            {
              foreach($fbResult as $result)
              {
                echo "<li>".$result['current_number']." == \"".$result['result']."\"</li>";
              }
            }
          ?>
          </ul>
        </div>
    </main>

    <footer class="mt-auto text-white-50">
        <p>Matthew Whitaker</p>
    </footer>
    </div>
  </body>
</html>
