<?php

use PHPUnit\Framework\TestCase;

class Test extends TestCase
{

    public function testNumbersClass()
    {
        require './class/numbers.class.php';

        for($i=1; $i<=1000; $i++)
        {
            // Check that the random numbers generated do not ever match each other
            $numbers = new Numbers();
            $numbers->generate();

            $this->assertNotEquals($numbers->number1, $numbers->number2, 'Numbers do not match, this is correct');

            // Check that the random numbers generated fit within constraints 0 < number <= 100
            $checkHigh = false;
            $checkLow = false;

            $checkHigh = $numbers->number1 <= 100 && $numbers->number2 <= 100;
            $checkLow = $numbers->number1 > 0 && $numbers->number2 > 0;

            $this->assertEquals(true, $checkHigh, 'Number is not too high');
            $this->assertEquals(true, $checkLow, 'Number is not too low');
        }
    }

    public function testAlgoClass()
    {
        require './class/algo.class.php';

        // Test that an array is sent back and test against an array that is already formed to give results between 1 and 15.
        $algo = new Algo(1, 15, false);
        $array = $algo->checkFizzBuzz();
        
        $arrayCheck = array(
            array('current_number' => 1, 'result' => 1),
            array('current_number' => 2, 'result' => 2),
            array('current_number' => 3, 'result' => 'Fizz'),
            array('current_number' => 4, 'result' => 4),
            array('current_number' => 5, 'result' => 'Buzz'),
            array('current_number' => 6, 'result' => 'Fizz'),
            array('current_number' => 7, 'result' => 7),
            array('current_number' => 8, 'result' => 8),
            array('current_number' => 9, 'result' => 'Fizz'),
            array('current_number' => 10, 'result' => 'Buzz'),
            array('current_number' => 11, 'result' => 11),
            array('current_number' => 12, 'result' => 'Fizz'),
            array('current_number' => 13, 'result' => 13),
            array('current_number' => 14, 'result' => 14),
            array('current_number' => 15, 'result' => 'FizzBuzz')
        );

        $this->assertEquals($arrayCheck ,$array, "Arrays match");
        $this->assertIsArray($array, "It is an array");

        // Check time constraint before and after noon
        $beforeNoon = $algo->checkTimeConstraint('11:59am');
        $afterNoon = $algo->checkTimeConstraint('12:59pm');

        $this->assertEquals(true, $beforeNoon, "Time should be before noon");
        $this->assertEquals(false, $afterNoon, "Time should be after noon");

        // Check stage 2 if time is before noon
        $algo2 = new Algo(1, 15, true);
        $algo2->beforeNoon = true;
        $array2 = $algo2->checkFizzBuzz();

        $arrayCheck2 = array(
            array('current_number' => 1, 'result' => 1),
            array('current_number' => 2, 'result' => 'Pling'),
            array('current_number' => 3, 'result' => 3),
            array('current_number' => 4, 'result' => 'Plong'),
            array('current_number' => 5, 'result' => 5),
            array('current_number' => 6, 'result' => 'Pling'),
            array('current_number' => 7, 'result' => 7),
            array('current_number' => 8, 'result' => 'Plong'),
            array('current_number' => 9, 'result' => 9),
            array('current_number' => 10, 'result' => 'Pling'),
            array('current_number' => 11, 'result' => 11),
            array('current_number' => 12, 'result' => 'Plong'),
            array('current_number' => 13, 'result' => 13),
            array('current_number' => 14, 'result' => 'Pling'),
            array('current_number' => 15, 'result' => 15)
        );

        $this->assertEquals($arrayCheck2, $array2, "Arrays match");
        $this->assertIsArray($array2, "It is an array");

        // Check stage 2 if time is after noon
        $algo3 = new Algo(1, 15, true);
        $algo3->beforeNoon = false;
        $array3 = $algo3->checkFizzBuzz();

        $arrayCheck3 = array(
            array('current_number' => 1, 'result' => 1),
            array('current_number' => 2, 'result' => 2),
            array('current_number' => 3, 'result' => 'Fizz'),
            array('current_number' => 4, 'result' => 4),
            array('current_number' => 5, 'result' => 'Buzz'),
            array('current_number' => 6, 'result' => 'Fizz'),
            array('current_number' => 7, 'result' => 7),
            array('current_number' => 8, 'result' => 8),
            array('current_number' => 9, 'result' => 'Fizz'),
            array('current_number' => 10, 'result' => 'Buzz'),
            array('current_number' => 11, 'result' => 11),
            array('current_number' => 12, 'result' => 'Fizz'),
            array('current_number' => 13, 'result' => 13),
            array('current_number' => 14, 'result' => 14),
            array('current_number' => 15, 'result' => 'FizzBuzz')
        );

        $this->assertEquals($arrayCheck3, $array3, "Arrays match");
        $this->assertIsArray($array3, "It is an array");
    }

}
