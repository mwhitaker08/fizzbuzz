<?php
include('./class/numbers.class.php');
include('./class/algo.class.php');

// If the form is submitted and a stage was chosen
if(isset($_POST) && isset($_POST['submit']) && !empty($_POST['stage']))
{
    // Generate a new set of numbers
    $createNums = new Numbers();
    $createNums->generate();

    $stage = true;
    if($_POST['stage'] == '1')
    {
        $stage = false;
    }
    elseif($_POST['stage'] == '2')
    {
        $stage = true;
    }

    // Send those two numbers to the algorithm class and run checkFizzBuzz()
    $fizzBuzz = new Algo($createNums->number1, $createNums->number2, $stage);
    $fbResult = $fizzBuzz->checkFizzBuzz();
}