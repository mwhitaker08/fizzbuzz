<?php

class Numbers
{
    public $number1;
    public $number2;

    // Generate two random numbers that are no equal between 1 and 100
    // If it is equal when randomly generated, keep generating a second number until it does not match the first number
    // Sort the numbers least to greatest and assign them to the object
    function generate()
    {
        $num1 = rand(1,100);
        $temp = rand(1,100);
        if($temp === $num1)
        {
            do
            {
                $temp = rand(1,100);
            }
            while ($temp === $num1);

            $num2 = $temp;
        }
        else
        {
            $num2 = $temp;
        }

        $numbers = [$num1, $num2];
        sort($numbers);

        $this->number1 = $numbers[0];
        $this->number2 = $numbers[1];
    }
}