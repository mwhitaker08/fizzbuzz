<?php

class Algo
{
    public $number1;
    public $number2;
    public $stage;
    public $beforeNoon;

    // Bring in the two numbers to loop through in the algorithm
    // Check stage to see if Pling Plong and time check is used
    function __construct($num1, $num2, $stage)
    {
        if(is_int($num1) && is_int($num2))
        {
            $this->number1 = $num1;
            $this->number2 = $num2;
        }
        else
        {
            return false;
        }

        if(is_bool($stage))
        {
            $this->stage = $stage;
        }
        else
        {
            return true;
        }
        
        if($stage !== false)
        {
            $this->beforeNoon = $this->checkTimeConstraint('now');
        }
    }

    // Check the time constraint of before noon and after 
    function checkTimeConstraint($time){
        date_default_timezone_set('US/Eastern');
        $currentTime = strtotime($time);

        return ($currentTime > strtotime('12:00am') && $currentTime < strtotime('12:00pm'));
    }

    // Using the two numbers, loop through each number incremented starting with the first number and ending with the last
    // If stage 2 is selected, and the time check is after noon for the day, do Fizz/Buzz/FizzBuzz results, else do Pling/Plong results
    // Push the results to a multi-dimensional array
    function checkFizzBuzz()
    {
        if(isset($this->number1) && isset($this->number2))
        {
            $algoArray = [];
            for($currentNum=$this->number1; $currentNum<=$this->number2; $currentNum++)
            {
                if(($this->stage !== false && $this->beforeNoon === false) || $this->stage === false)
                {
                    if($currentNum % 15 === 0)
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => 'FizzBuzz'));
                    }
                    elseif($currentNum % 5 === 0)
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => 'Buzz'));
                    }
                    elseif($currentNum % 3 === 0)
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => 'Fizz'));
                    }
                    else
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => $currentNum));
                    }
                }
                else
                {
                    if($currentNum % 4 === 0)
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => 'Plong'));
                    }
                    elseif($currentNum % 2 === 0)
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => 'Pling'));
                    }
                    else
                    {
                        array_push($algoArray, array('current_number' => $currentNum, 'result' => $currentNum));
                    }
                }
            }
            return $algoArray;
        }
        else
        {
            return false;
        }
    }
}